/*
	*
	* This file is a part of CoreArchiver.
	* Archive manager for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, vsit http://www.gnu.org/licenses/.
	*
*/

#include "carchivemodel.hpp"

#include <archive.h>
#include <archive_entry.h>
#include <cprime/filefunc.h>

static QMimeDatabase mimeDb;

static void growBranch(const QString &path, CArchiveBranch *parent)
{
	if (not QFileInfo::exists(path)) {
		return;
	}

	QDirIterator it(path, QDir::AllEntries | QDir::System | QDir::NoDotAndDotDot | QDir::Hidden, QDirIterator::NoIteratorFlags);

	while (it.hasNext()) {
		it.next();
		QFileInfo fi(it.fileInfo());
		QString name = /*(fi.isDir() ? fi.absolutePath() :*/ fi.fileName()/*)*/;
		QString size = (fi.isDir() ? "" : CPrime::FileUtils::formatSize(fi.size()));
		QMimeType mime = mimeDb.mimeTypeForFile(fi.filePath());

		QString iconName = mime.iconName();
		CArchiveBranch *branch = new CArchiveBranch(name, QIcon::fromTheme(iconName), size, parent);
		parent->addBranch(branch);

		if (fi.isDir()) {
			growBranch(it.filePath(), branch);
		}

		qApp->processEvents();
	}
}

/* Walk all the nodes of a branch: branchpath is appended to all subsequent nodes */
/* @parent will not be a part of this list, also folders will not be listed */
static QStringList walkTree(const QString &branchPath, CArchiveBranch *parent)
{
	QStringList fileList;
	QDir currBranch(branchPath);

	Q_FOREACH (auto *branch, parent->branches()) {
		/* If this branch is further branched out - it is a folder */
		if (branch->branchCount()) {
			fileList << walkTree(currBranch.filePath(branch->name()), branch);
		} else {
			fileList << currBranch.filePath(branch->name());
		}

		qApp->processEvents();
	}

	return fileList;
}

CArchiveModel::CArchiveModel() : QAbstractItemModel()
{
	tree = new CArchiveBranch("Name", QIcon::fromTheme("folder"), 0);
}

CArchiveModel::~CArchiveModel()
{
	delete tree;
}

void CArchiveModel::loadArchive(const QString &path)
{
	/* Archive name and path */
	archiveName = path;

	/* Root node */
	growTree();
}

void CArchiveModel::setArchiveName(const QString &path)
{
	archiveName = path;
}

int CArchiveModel::rowCount(const QModelIndex &parent) const
{
	CArchiveBranch *parentItem;

	if (parent.column() > 0) {
		return 0;
	}

	if (not parent.isValid()) {
		parentItem = tree;
	} else {
		parentItem = static_cast<CArchiveBranch *>(parent.internalPointer());
	}

	return parentItem->branchCount();
}

int CArchiveModel::columnCount(const QModelIndex &) const
{
	return 2;
}

Qt::ItemFlags CArchiveModel::flags(const QModelIndex &idx) const
{
	if (not idx.isValid()) {
		return Qt::NoItemFlags;
	}

	return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QVariant CArchiveModel::data(const QModelIndex &index, int role) const
{
	if (not index.isValid()) {
		return QVariant();
	}

	CArchiveBranch *node = static_cast<CArchiveBranch *>(index.internalPointer());

	switch (role) {

		case Qt::DisplayRole: {
			if (index.column() == 0) {
				return node->name();
			} else if (index.column() == 1) {
				return node->size();
			}

			return QVariant();
		}

		case Qt::DecorationRole:
			if (index.column() == 0) {
				return node->icon();

				return QVariant();
			}

		case Qt::TextAlignmentRole: {
			if (index.column() == 0) {
				return (0x0001 | 0x0080); // Qt::AlignLeft | Qt::AlignVCenter
			} else if (index.column() == 1) {
				return (0x0002 | 0x0080); // Qt::AlignRight | Qt::AlignVCenter
			} else {
				return Qt::AlignCenter;
			}
		}

		case Qt::InitialSortOrderRole: {
			return Qt::AscendingOrder;
		}

		case Qt::AccessibleTextRole: {
			return node->name();
		}

		case Qt::ToolTipRole: {
			if (index.column() == 0) {
				return node->data(Qt::UserRole + 1);
			}

			return QString();
		}

		case Qt::UserRole + 1: {
			return node->data(Qt::UserRole + 1);
		}

		default: {
			return QVariant();
		}
	}
}

QVariant CArchiveModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (orientation != Qt::Horizontal) {
		return QVariant();
	}

	switch (section) {
		case 0: {
			switch (role) {
				case Qt::DisplayRole:
					return "Name";

				case Qt::TextAlignmentRole:
					return Qt::AlignCenter;

				default:
					return QVariant();
			}
		}

		case 1: {
			switch (role) {
				case Qt::DisplayRole:
					return "Size";

				case Qt::TextAlignmentRole:
					return Qt::AlignCenter;

				default:
					return QVariant();
			}
		}

		default: {
			return QVariant();
		}
	}
}

QModelIndex CArchiveModel::index(int row, int column, const QModelIndex &parent) const
{
	if (row < 0 or column < 0) {
		return QModelIndex();
	}

	if ((row > rowCount(parent)) and (column > columnCount(parent))) {
		return QModelIndex();
	}

	CArchiveBranch *parentNode;

	if (not parent.isValid()) {
		parentNode = tree;
	} else {
		parentNode = static_cast<CArchiveBranch *>(parent.internalPointer());
	}

	CArchiveBranch *branch = parentNode->branch(row);

	if (branch) {
		return createIndex(row, column, branch);
	} else {
		return QModelIndex();
	}
}

QModelIndex CArchiveModel::index(QString name, const QModelIndex &parent) const
{
	CArchiveBranch *parentNode;

	if (not parent.isValid()) {
		parentNode = tree;
	} else {
		parentNode = static_cast<CArchiveBranch *>(parent.internalPointer());
	}

	CArchiveBranch *branch = parentNode->branch(name);

	if (branch) {
		return createIndex(branch->row(), 0, branch);
	}

	return QModelIndex();
}

QModelIndex CArchiveModel::parent(const QModelIndex &idx) const
{
	if (not idx.isValid()) {
		return QModelIndex();
	}

	CArchiveBranch *branch = static_cast<CArchiveBranch *>(idx.internalPointer());
	CArchiveBranch *parentNode = branch->parent();

	if (parentNode == tree) {
		return QModelIndex();
	}

	return createIndex(parentNode->row(), 0, parentNode);
}

bool CArchiveModel::hasBranches(const QModelIndex &idx) const
{
	if (not idx.isValid()) {
		return true;
	} else {
		CArchiveBranch *branch = static_cast<CArchiveBranch *>(idx.internalPointer());
		return (branch->branchCount() > 0);
	}
}

void CArchiveModel::selectBranches(QStringList)
{

}

void CArchiveModel::growBranch(QStringList files)
{
	beginResetModel();
	emit archiveLoading();

	Q_FOREACH (QString file, files) {
		QFileInfo fi(file);

		QString size = (fi.isDir() ? "" : CPrime::FileUtils::formatSize(fi.size()));
		QMimeType mime = mimeDb.mimeTypeForFile(fi.filePath());

		QString iconName = mime.iconName();
		CArchiveBranch *branch = new CArchiveBranch(file, QIcon::fromTheme(iconName), size, tree);
		tree->addBranch(branch);

		if (fi.isDir()) {
			::growBranch(file, branch);
		}
	}

	mModified = true;
	emit archiveLoaded();

	endResetModel();
}

void CArchiveModel::chopBranch(QModelIndexList indexes)
{
	emit archiveLoading();

	Q_FOREACH (QModelIndex idx, indexes) {
		beginRemoveRows(idx.parent(), idx.row(), idx.row());

		CArchiveBranch *parent;
		CArchiveBranch *branch = static_cast<CArchiveBranch *>(idx.internalPointer());

		if (branch) {
			parent = branch->parent();
		} else {
			continue;
		}

		parent->removeBranch(branch);

		endRemoveRows();
	}

	mModified = true;
	emit archiveLoaded();
}

bool CArchiveModel::isArchiveModified()
{
	return mModified;
}

QStringList CArchiveModel::filePaths()
{
	return walkTree(QString(), tree);
}

QString CArchiveModel::nodeName(const QModelIndex idx) const
{
	return idx.data(0).toString();
}

QString CArchiveModel::nodePath(const QModelIndex idx) const
{
	if (not idx.isValid()) {
		return QString();
	}

	QString path = idx.data().toString();
	QModelIndex par = parent(idx);

	while (par.isValid()) {
		path = par.data().toString() + "/" + path;
		par = parent(par);
	}

	return path;
}

void CArchiveModel::clear()
{
	beginResetModel();
	tree->clearBranches();
	endResetModel();
}

void CArchiveModel::growTree()
{
	beginResetModel();

	if (CPrime::FileUtils::exists(archiveName)) {
		LibArchiveQt *archive = new LibArchiveQt(archiveName);

		emit archiveLoading();

		Q_FOREACH (ArchiveEntry *ae, archive->listArchive()) {
			QString name = QString(ae->name);
			int type = ae->type;
			QString size = CPrime::FileUtils::formatSize(ae->size);

			QStringList tokens = QStringList() << name.split("/", Qt::SkipEmptyParts);
			QString iconStr = ((type == AE_IFDIR) ? "folder" : mimeDb.mimeTypeForFile(tokens.value(0)).iconName());
			tree->addBranch(new CArchiveBranch(
								tokens.value(0),
								QIcon::fromTheme(iconStr),
								((type == AE_IFDIR) ? "" : size), tree));

			if (tokens.size() == 1) {
				continue;
			}

			CArchiveBranch *branch = tree;

			for (int i = 1; i < tokens.size(); i++) {
				branch = branch->branch(tokens.value(i - 1));
				QString iconStr_2 = ((type == AE_IFDIR) ? "folder" : mimeDb.mimeTypeForFile(tokens.value(i)).iconName());

				branch->addBranch(new CArchiveBranch(
									  tokens.value(i),
									  QIcon::fromTheme(iconStr_2),
									  ((type == AE_IFDIR) ? "" : size), branch));
			}

			qApp->processEvents();
		}

		emit archiveLoaded();
	}

	endResetModel();
}
