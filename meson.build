project(
    'CoreArchiver',
    'c',
	'cpp',
	version: '5.0.0',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-DVERSION_TEXT="@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_arguments( '-DAUDIO_RESOURCE_FOLDER="@0@"'.format( get_option( 'datadir' ) + '/coreapps/resource' ), language : 'cpp' )

add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

GlobalInc = include_directories( [ '.', 'src' ] )

Qt = import( 'qt6' )
QtDeps = dependency(
	'qt6',
	modules: [ 'Core', 'Gui', 'Widgets', ]
)

CPrime = dependency( 'cprime-widgets-qt6' )

ArchiveQt = dependency( 'archiveqt6' )

Headers = [
    'src/settings.h',
]

MocHeaders = [
    'src/carchivebranch.hpp',
    'src/carchivemodel.hpp',
    'src/corearchiver.h',
]

Sources = [
    'src/carchivebranch.cpp',
    'src/carchivemodel.cpp',
    'src/corearchiver.cpp',
    'src/main.cpp',
    'src/settings.cpp',
]

Mocs = Qt.compile_moc(
    headers : MocHeaders,
    dependencies: QtDeps
)

UIs = Qt.compile_ui(
    sources: [
        'src/corearchiver.ui',
    ]
)

corearchiver = executable(
    'corearchiver', [ Sources, Mocs, UIs ],
    dependencies: [QtDeps, CPrime, ArchiveQt],
    include_directories: GlobalInc,
    install: true
)

install_data(
    'cc.cubocore.CoreArchiver.desktop',
    install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'applications' )
)

install_data(
    'cc.cubocore.CoreArchiver.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)
